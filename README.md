# LRS Grid Editor Module
The LRS Grid Editor Module provides a component which edits LRS data in an ESRI map instance in a custom way.

## Functionality
The difference between the components in this module and the standard grid editing tools found in RCE and AngularUI is
that instead of merely editing data records directly, the components define an editing process which allows a history
to be kept.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-lrs-grid-editor.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-lrs-grid-editor/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-lrs-grid-editor](http://code.tsstools.com/bower-lrs-grid-editor)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.lrs-grid-editor']);
```

## To Do
- Complete 100% unit test coverage.